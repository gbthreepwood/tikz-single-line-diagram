# TikZ single line diagram

A LaTeX/TikZ library for drawing single line diagrams for electical power systems.

![](doc/img/radial_power_system_example.png)
